//Cette fonction sera appelée au chargement de la page, comme ça on est sur que tous
//les objets (div, canvas etc.) sont bien présents
$(function(){
	//On récupère notre canvas
	var canvas = $("#canvas").get(0);
	var width = 640;
	var height = 480;
	//On détermine manuellement la taille du canvas
	canvas.width = width;
	canvas.height = height;
	//Puis le "context" qui nous permet de faire de la 2d dessus
	//Si on avait mis 3d, on pourrait faire du WebGL mais ce n'est pas notre volonté
	var ctx = canvas.getContext("2d");
	//On commence par dessiner un rectangle blanc de la taille de notre canvas
	ctx.fillStyle = "#FFF";
	//Le 0, 0 c'est la position du point en haut à gauche du canvas
	//width et height, c'est la largeur et la hauteur du rectangle
	//Ici la taille de notre canvas
	ctx.fillRect(0, 0, width, height);

	//Faisons une fonction pour dessiner une ligne
	function drawLine(x1, y1, x2, y2){
		//beginPath signifie que l'on commence à dessiner un "chemin"
		//Qui sera consitué ici de 2 points et qui formera donc une ligne
		ctx.beginPath();
		//moveTo signifie que l'on déplace notre outil de dessin à l'endroit x1, y1
		//Qui sont des coordonnées en pixel et dont le 0,0 est le coin en haut à gauche du canvas
		ctx.moveTo(x1, y1);
		//lineTo signifie que l'on déplace notre outil jusqu'au point x2, y2 suivant une ligne
		ctx.lineTo(x2, y2);
		//Maintenant on dessine cette ligne.
		//Stroke signifie en anglais "trait", "coup de pinceau" et désigne dans le canvas les contours d'un objet
		//Pour une ligne il s'agit juste de la ligne mais pour un rectangle ou un rond c'est l'equivalent du border css
		//Le fill c'est pour le contenu (le remplissage), l'équivalent du background css en gros.
		ctx.stroke();
		//Comme on a commencé le chemin, il faut le finir. Le closePath sert à ça.
		//Essayez de l'enlever pour voir :)
		ctx.closePath();
	}
	
	//Bon, on sait récupérer la position de notre souris pour nos deux clics
	//Il nous reste plus qu'à tracer notre ligne sur le deuxième clic
	//mais que faut-il pour cela ?
	//Il faut que l'on stocke la position du premier clic en dehors de la fonction
	//sinon on ne va plus pouvoir la retrouver
	//Je les initialise à 0
	var x1 = 0;
	var y1 = 0;
	var firstClick = true;
	$("#canvas").on("click", function(event){
		var x = event.pageX - canvas.offsetLeft;
		var y = event.pageY - canvas.offsetTop;
		if(firstClick){
			//On les stocke pour ne pas les oublier
			x1 = x;
			y1 = y;
			firstClick = false;
		}
		else{
			//Il nous reste plus qu'à tracer la ligne, trop easy t'as vu !!!
			drawLine(x1, y1, x, y);
			//Et je repasse ma variable à true comme ça je peux faire ça indéfiniment
			firstClick = true;
		}
		
	});
});